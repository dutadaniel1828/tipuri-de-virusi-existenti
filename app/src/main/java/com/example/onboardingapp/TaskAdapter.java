package com.example.onboardingapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class TaskAdapter extends ArrayAdapter {
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;

    private CheckBox taskSeen;
    private int myId;

    private int resursaID;
    public TaskAdapter(Context context, int resource, List<Task> taskuri) {
        super(context, resource, taskuri);
        this.resursaID=resource;
    }

    //aceasta metoda se va apela pentru fiecare obiect din lista de taskuri
    //position spune pentru al catelea element s-a apelat

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //imi obtine item de pe pozitia position
        final Task task = (Task)getItem(position);
        //aici se creeaza layout-ul
        LayoutInflater li = LayoutInflater.from(getContext());
        View view = li.inflate(resursaID,null);
        //momentan view-ul este gol
        //trebuie sa il populam cu campurile din acel task
        TextView taskId = view.findViewById(R.id.task_id);
        taskId.setText("" + task.getId());

        ImageView imageType = view.findViewById(R.id.task_tip_atac);
        if(task.getTip_atac().equals("APT attack"))
            imageType.setImageResource(R.drawable.apt);
        else if (task.getTip_atac().equals("Cyber attack"))
            imageType.setImageResource(R.drawable.cyber);
        else if (task.getTip_atac().equals("Zero Day attack"))
            imageType.setImageResource(R.drawable.zero);
        else if (task.getTip_atac().equals("Phishing attack"))
            imageType.setImageResource(R.drawable.phishing);
        else if (task.getTip_atac().equals("Brute Force attack"))
            imageType.setImageResource(R.drawable.brute);
        else
            imageType.setImageResource(R.drawable.known);

        TextView taskName = view.findViewById(R.id.task_name);
        if(task.getName().length() > 9)
            taskName.setText(task.getName().substring(0,9) + "...");
        else
            taskName.setText(task.getName());

        TextView taskScore = view.findViewById(R.id.task_score);
        taskScore.setText("" + task.getScore());

        TextView taskDuration = view.findViewById(R.id.task_duration);
        taskDuration.setText("" + task.getDurata());

        //Pentru shared preferences
        myId = task.getId();
        taskSeen = view.findViewById(R.id.task_seen);
        //taskSeen.setChecked(task.isSeen());
        final String identifier = "checkbox"+myId;
        mPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        mEditor = mPreferences.edit();

        checkSharedPreferences();

        taskSeen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()){
                    //Memoram starea checkboxului
                    mEditor.putString(identifier,"True");
                    mEditor.commit();
                }else{
                    mEditor.putString(identifier,"False");
                    mEditor.commit();
                }
            }
        });

        /////////////

        Button task_button = view.findViewById(R.id.task_button);
        task_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(getContext(),taskPage.class);
                it.putExtra("myTask",task);
                getContext().startActivity(it);
            }
        });


        return view;
    }

    //Verificam shared preferences
    private void checkSharedPreferences(){
        String identifier = "checkbox"+myId;
        String checkbox = mPreferences.getString(identifier,"False");

        if(checkbox.equals("True")){
            taskSeen.setChecked(true);
        }else{
            taskSeen.setChecked(false);
        }
    }
}
