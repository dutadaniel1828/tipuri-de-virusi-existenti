package com.example.onboardingapp;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

import java.util.List;

public class DesenareFileType extends View {
    List<String> fileTypeList;

    public DesenareFileType(Context context, List<String> fileTypeList) {
        super(context);
        this.fileTypeList = fileTypeList;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Paint p = new Paint();
        float valoare;

        int culoare = 20;
        float increment = 0;

        int txt = 0;
        int pdf = 0;

        for(int i = 0 ; i < fileTypeList.size(); i++){
            if(fileTypeList.get(i).equals(getResources().getString(R.string.ASCII_text)))
                txt++;
            else
                pdf ++;
        }
        int suma = txt + pdf;

        String format = "";
        int start = 100;
        int start_legend = 200;

        Paint p_legenda = new Paint();
        p_legenda.setStrokeWidth(100);

        Paint p_text = new Paint();
        p_text.setColor(Color.RED);
        p_text.setTextSize(50);




        for(int i =0 ;i < 2; i++){
            if (i == 0){
                valoare = (float)(txt*360/suma);
                format = getResources().getString(R.string.txt);
            }
            else{
                valoare = (float)(pdf*360/suma);
                format = getResources().getString(R.string.pdf);
            }
            p.setColor( Color.rgb((300*culoare)%256,culoare%256,(20*culoare)%256));
            p_legenda.setColor( Color.rgb((300*culoare)%256,culoare%256,(20*culoare)%256));
            canvas.drawArc(200, 800, 800, 1400, increment, valoare, true, p);

            //pentru legenda
            canvas.drawLine(start,start_legend, start ,start_legend + 50,p_legenda);
            canvas.drawText(format,start +100,start_legend + 40,p_text);

            start_legend += 100;

            increment += valoare;
            culoare +=50;
        }


    }
}
