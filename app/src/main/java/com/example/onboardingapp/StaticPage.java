package com.example.onboardingapp;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class StaticPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static_page);

        Intent it = this.getIntent();
        Bundle b = it.getExtras();

        cuckooTask myTask = (cuckooTask) b.getParcelable("myTask");

        //Pentru imaginea cu tipul de malware
        ImageView image_malware_type = findViewById(R.id.imageView_malware_type);
        if(myTask.getMalware_type().equals("APT attack"))
            image_malware_type.setImageResource(R.drawable.apt);
        else if (myTask.getMalware_type().equals("Cyber attack"))
            image_malware_type.setImageResource(R.drawable.cyber);
        else if (myTask.getMalware_type().equals("Zero Day attack"))
            image_malware_type.setImageResource(R.drawable.zero);
        else if (myTask.getMalware_type().equals("Phishing attack"))
            image_malware_type.setImageResource(R.drawable.phishing);
        else if (myTask.getMalware_type().equals("Brute Force attack"))
            image_malware_type.setImageResource(R.drawable.brute);
        else
            image_malware_type.setImageResource(R.drawable.known);

        //Pentru imaginea cu tipul de fisier
        ImageView image_file_type = findViewById(R.id.imageView_file_type);
        if(myTask.getFile_type().equals("ASCII text"))
            image_file_type.setImageResource(R.drawable.filetype_txt);
        else
            image_file_type.setImageResource(R.drawable.filetype_pdf);

        TextView name = findViewById(R.id.textView_nume);
        name.setText(myTask.getName());

        TextView size = findViewById(R.id.textView_size);
        size.setText("" + myTask.getSize());
    }
}
