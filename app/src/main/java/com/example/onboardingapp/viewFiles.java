package com.example.onboardingapp;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class viewFiles extends AppCompatActivity {
    //Pentru desen cu scoruri
    List<Integer> durationList = new ArrayList<>();
    List<String> nameList = new ArrayList<>();
    List<String> fileTypeList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_files);

        //adapter.deleteRows();

        //Se creeaza obiectul cuckooTask cu date din json
        for(int i = 0; i<= 5; i++) {
            GetJson json = new GetJson() {
                @Override
                protected void onPostExecute(cuckooTask task) {
                    super.onPostExecute(task);

                    //inseram task-ul in baza de date
                    //adapter pentru baza de date
                    DBAdapter adapter = new DBAdapter(getApplicationContext());
                    adapter.openConnection();
                    adapter.insertReport(task);
                    adapter.closeConenction();

                    //Toast.makeText(viewFiles.this, "" + task.getName(), Toast.LENGTH_SHORT).show();
                }
            };
            json.execute("http://10.0.2.2:3000/reports/" + i);
        }


        DBAdapter adapterDB = new DBAdapter(this);
        adapterDB.openConnection();
        List<cuckooTask> lista = new ArrayList<>();
        lista = adapterDB.selectToateRapoartele();
        adapterDB.closeConenction();

        //sortam lista
        int durata1 = 0;
        int durata2 = 0;
        for(int i = 0; i < lista.size()-1; i++){
            for(int j = i+1; j < lista.size(); j++){
                durata1 = lista.get(i).getDuration();
                durata2 = lista.get(j).getDuration();
                if( durata1 > durata2 ){
                    cuckooTask auxiliar = new cuckooTask(lista.get(i).getId(),lista.get(i).getDuration(),lista.get(i).getScore(),lista.get(i).getLocation1(),lista.get(i).getLocation2(),lista.get(i).getMalware_type(),lista.get(i).getName(),lista.get(i).getFile_type(),lista.get(i).getSize());
                    lista.set(i,lista.get(j));
                    lista.set(j,auxiliar);
                }
            }
        }

        //adaugam in lista
        List<Task> taskuri = new ArrayList<>();

        for(int i = 0; i < lista.size(); i++) {
            durationList.add(lista.get(i).getDuration());
            nameList.add(lista.get(i).getName());
            fileTypeList.add(lista.get(i).getFile_type());
            taskuri.add(new Task(lista.get(i).getId(), lista.get(i).getName(), lista.get(i).getMalware_type(), lista.get(i).getScore(), lista.get(i).getDuration(), false));
        }

        //identificam listview-ul nostru
        ListView lv = findViewById(R.id.listViewReports);
        //se creeaza una adapter pentru listView
        TaskAdapter adapter = new TaskAdapter(this,R.layout.task_layout,taskuri);
        //setam adapterul listView-ului nostru
        lv.setAdapter(adapter);
    }

    public class GetJson extends AsyncTask<String, Void, cuckooTask>{
        @Override
        protected cuckooTask doInBackground(String... strings) {
            cuckooTask task = new cuckooTask();
            try {
                URL url = new URL(strings[0]);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = httpURLConnection.getInputStream();

                //Pentru a citii linie cu linie
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                String linie = null;
                StringBuilder builder = new StringBuilder();
                //cand ajunge la final ne returneaza null
                while((linie = reader.readLine()) != null){
                    builder.append(linie);
                }
                //prima data e obiect {}
                //JSONArray vector = new JSONArray(builder.toString());
                //acuma gasim obiect
                JSONObject object= new JSONObject(builder.toString());
                //luam id :
                task.setId(object.getInt("id"));
                //luam informatiile din info
                JSONObject info = object.getJSONObject("info");
                task.setDuration(info.getInt("duration"));
                task.setScore(info.getDouble("score"));
                task.setLocation1(info.getDouble("location1"));
                task.setLocation2(info.getDouble("location2"));
                task.setMalware_type(info.getString("type"));

                //luam informatiile din target
                JSONObject target = object.getJSONObject("target");
                JSONObject file = target.getJSONObject("file");
                task.setName(file.getString("name"));
                task.setFile_type(file.getString("type"));
                task.setSize(file.getInt("size"));

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return task;
        }
    }

    public void viewDurationPychart(View view){
        setContentView(new DesenareDuration(this,durationList,nameList));
    }

    public void viewFileTypePychart(View view){
        setContentView(new DesenareFileType(this,fileTypeList));
    }
}
