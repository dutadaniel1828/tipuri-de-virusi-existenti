package com.example.onboardingapp;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private ViewPager mSlideViewPager;
    private LinearLayout mDotLayout;

    private TextView[] mDots;

    private SlideAdapter sliderAdapter;

    private int pageSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSlideViewPager = (ViewPager) findViewById(R.id.slideViewPager);
        mDotLayout = (LinearLayout) findViewById(R.id.dotsLayout);

        //initializam clasa sliderAdapter
        sliderAdapter = new SlideAdapter(this);

        mSlideViewPager.setAdapter(sliderAdapter);

        //setam pagina de pornire 0
        addDotsIndicator(0);

        mSlideViewPager.addOnPageChangeListener(viewListener);
    }


    public void addDotsIndicator(int position){
        mDots = new TextView[2];
        //ca sa nu creeze un numar multiplu de puncte
        mDotLayout.removeAllViews();
        for(int i = 0; i < mDots.length; i ++){
            mDots[i] = new TextView(this);
            mDots[i].setText(Html.fromHtml("&#8226;"));
            mDots[i].setTextSize(35);
            mDots[i].setTextColor(getResources().getColor(R.color.colorTransparentWhite));

            mDotLayout.addView(mDots[i]);
        }

        if(mDots.length > 0){
            mDots[position].setTextColor(getResources().getColor(R.color.colorWhite));
        }
    }

    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {

        }

        @Override
        public void onPageSelected(int i) {
            addDotsIndicator(i);
            pageSelected = i;
        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };


    public void viewAnalyzedFiles(View view){
        Intent viewFiles = new Intent(this, viewFiles.class);
        Intent shortHistory = new Intent(this, history.class);
        //Pentru tutorial
        if (pageSelected == 0)
            startActivity(shortHistory);
        else{
            //Pentru fisierele analizate
            startActivity(viewFiles);
        }
    }
}
