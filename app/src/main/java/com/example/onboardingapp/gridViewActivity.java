package com.example.onboardingapp;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

public class gridViewActivity extends AppCompatActivity {
    TextView text;
    TextView textContent;
    ImageView image;
    RelativeLayout background;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view);

        text = findViewById(R.id.textView_gridActivity);
        image = findViewById(R.id.imageView_gridActivity);
        background = findViewById(R.id.background_gridView_activity);
        textContent = findViewById(R.id.textView_gridActivity_content);

        Intent intent = getIntent();
        text.setText(intent.getStringExtra("name"));
        textContent.setText(intent.getIntExtra("content",0));

        image.setImageResource(intent.getIntExtra("image",0));
        background.setBackgroundColor(Color.parseColor(intent.getStringExtra("color")));

    }
}
