package com.example.onboardingapp;

import android.os.Parcel;
import android.os.Parcelable;

public class Task implements Parcelable {
    private int id;
    private String name;
    private String tip_atac;
    private double score;
    private int durata;
    private boolean seen;

    public Task(int id, String name, String tip_atac, double score, int durata, boolean seen) {
        this.id = id;
        this.name = name;
        this.tip_atac = tip_atac;
        this.score = score;
        this.durata = durata;
        this.seen = seen;
    }

    protected Task(Parcel in) {
        id = in.readInt();
        name = in.readString();
        tip_atac = in.readString();
        score = in.readDouble();
        durata = in.readInt();
        seen = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(tip_atac);
        dest.writeDouble(score);
        dest.writeInt(durata);
        dest.writeByte((byte) (seen ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Task> CREATOR = new Creator<Task>() {
        @Override
        public Task createFromParcel(Parcel in) {
            return new Task(in);
        }

        @Override
        public Task[] newArray(int size) {
            return new Task[size];
        }
    };


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTip_atac() {
        return tip_atac;
    }

    public void setTip_atac(String tip_atac) {
        this.tip_atac = tip_atac;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public int getDurata() {
        return durata;
    }

    public void setDurata(int durata) {
        this.durata = durata;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }


    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", tip_atac='" + tip_atac + '\'' +
                ", score=" + score +
                ", durata=" + durata +
                ", seen=" + seen +
                '}';
    }
}
