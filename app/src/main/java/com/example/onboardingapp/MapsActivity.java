package com.example.onboardingapp;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private List<LatLng> pozVeche  ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);

        //aduagam eveniment cand dam click pe harta sa adauge un nou marker

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                mMap.addMarker(new MarkerOptions().position(latLng));
                //centram camera pe marker
                //mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                //ca sa se miste smooth
                mMap.animateCamera(CameraUpdateFactory.newLatLng((latLng)));

                //LatLng atm2=new LatLng(44.418970, 26.087078);
                //mMap.addMarker(new MarkerOptions().position(atm2));
                //adaugam o linie
                //mMap.addPolyline(new PolylineOptions().add(pozVeche).add(latLng));
                for(int i = 0 ; i < pozVeche.size(); i++)
                    mMap.addPolygon(new PolygonOptions().add(pozVeche.get(i)).add(latLng));
                pozVeche.add(latLng);
            }
        });

        Intent it = this.getIntent();
        Bundle b = it.getExtras();

        cuckooTask myTask = (cuckooTask) b.getParcelable("myTask");


        // Add a marker in Sydney and move the camera
        LatLng atm = new LatLng(myTask.getLocation1(), myTask.getLocation2());
        Marker marker = mMap.addMarker(new MarkerOptions().position(atm).title("Marker in Sydney"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(atm));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(atm,18));
        pozVeche = new ArrayList<LatLng>();
        pozVeche.add(atm);
    }
}
