package com.example.onboardingapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    //Se va creea baza de date daca nu exista
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table "
                + Constante.NUME_TABELA
                + " ("+Constante.ID+" integer, "
                +Constante.DURATION+" integer,"
                +Constante.SCORE+" real,"
                +Constante.LOCATION1+" real,"
                +Constante.LOCATION2+" real,"
                +Constante.MALWARE_TYPE+" text,"
                +Constante.NAME+" text,"
                +Constante.FILE_TYPE+" text,"
                +Constante.SIZE+" integer);");
    }

    //se apeleaza cand vrem, sa modificam
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //mai intai stergem tabela
        db.execSQL("drop table if exists "+Constante.NUME_TABELA);
        onCreate(db);
    }
}
