package com.example.onboardingapp;

//Definim numele tabelei si a coloanelor
public class Constante {
    public final static String NUME_TABELA="CuckooReports";
    public final static String ID = "id";
    public final static String DURATION="duration";
    public final static String SCORE="score";
    public final static String LOCATION1="location1";
    public final static String LOCATION2="location2";
    public final static String MALWARE_TYPE="malwareType";
    public final static String NAME="name";
    public final static String FILE_TYPE="fileType";
    public final static String SIZE="size";
    public final static String DBNAME="DBReports";
}
