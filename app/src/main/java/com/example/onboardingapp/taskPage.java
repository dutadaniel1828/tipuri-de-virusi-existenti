package com.example.onboardingapp;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class taskPage extends AppCompatActivity {
    protected int idTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_page);

        Intent it = this.getIntent();
        Bundle b = it.getExtras();

        Task myTask = (Task) b.getParcelable("myTask");

        ImageView image = findViewById(R.id.imageView);
        if(myTask.getTip_atac().equals("APT attack"))
            image.setImageResource(R.drawable.apt);
        else if (myTask.getTip_atac().equals("Cyber attack"))
            image.setImageResource(R.drawable.cyber);
        else if (myTask.getTip_atac().equals("Zero Day attack"))
            image.setImageResource(R.drawable.zero);
        else if (myTask.getTip_atac().equals("Phishing attack"))
            image.setImageResource(R.drawable.phishing);
        else if (myTask.getTip_atac().equals("Brute Force attack"))
            image.setImageResource(R.drawable.brute);
        else
            image.setImageResource(R.drawable.known);

        TextView id = findViewById(R.id.textView_id);
        id.setText(""+myTask.getId());
        idTask = myTask.getId();

        TextView name = findViewById(R.id.textView_name);
        name.setText(myTask.getName());

        RatingBar score = findViewById(R.id.ratingBar_score);
        score.setRating(Float.valueOf(String.valueOf(myTask.getScore())));

        TextView duration = findViewById(R.id.textView_duration);
        duration.setText("" + myTask.getDurata());

    }

    public void viewStatic(View view){
        //fac querry si trimit catre alta pagina
        DBAdapter adapterDB = new DBAdapter(this);
        adapterDB.openConnection();
        cuckooTask task = adapterDB.selectareTaskById(idTask);
        adapterDB.closeConenction();

        //Deschid pagina de Static Report
        Intent intent = new Intent(getApplicationContext(),StaticPage.class);
        intent.putExtra("myTask",task);
        startActivity(intent);
    }

    public void viewBehavior(View view){
        //fac querry si trimit catre alta pagina
        DBAdapter adapterDB = new DBAdapter(this);
        adapterDB.openConnection();
        cuckooTask task = adapterDB.selectareTaskById(idTask);
        adapterDB.closeConenction();

        //Deschid pagina de Static Report
        Intent intent = new Intent(getApplicationContext(),BehaviorPage.class);
        intent.putExtra("myTask",task);
        startActivity(intent);
    }

    public void viewMap(View view){
        //fac querry si trimit catre alta pagina
        DBAdapter adapterDB = new DBAdapter(this);
        adapterDB.openConnection();
        cuckooTask task = adapterDB.selectareTaskById(idTask);
        adapterDB.closeConenction();

        //Deschid pagina de Google Map
        Intent it = new Intent(getApplicationContext(),MapsActivity.class);
        it.putExtra("myTask",task);
        startActivity(it);
    }
}
