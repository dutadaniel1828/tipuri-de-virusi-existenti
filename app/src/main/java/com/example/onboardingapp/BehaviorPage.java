package com.example.onboardingapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class BehaviorPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_behavior_page);

        Intent it = this.getIntent();
        Bundle b = it.getExtras();

        cuckooTask myTask = (cuckooTask) b.getParcelable("myTask");

        //fac reqeust json pentru a lua lista de fisiere si comenzi deschise dupa id

        GetJson_fileOpened json = new GetJson_fileOpened() {
            @Override
            protected void onPostExecute(List<String> file_opened) {
                super.onPostExecute(file_opened);

                //Le adaug intr-un listView
                ListView listView_fileOpened = (ListView) findViewById(R.id.listView_fileOpened);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_1, file_opened);
                listView_fileOpened.setAdapter(adapter);
            }
        };
        json.execute("http://10.0.2.2:3000/reports/" + myTask.getId());


        //TextView name = findViewById(R.id.textView_nume);
        //name.setText(myTask.getName());

    }


    public class GetJson_fileOpened extends AsyncTask<String, Void, List<String>> {
        @Override
        protected List<String> doInBackground(String... strings) {
            List<String> fileOpened = new ArrayList<>();
            try {
                URL url = new URL(strings[0]);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = httpURLConnection.getInputStream();

                //Pentru a citii linie cu linie
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                String linie = null;
                StringBuilder builder = new StringBuilder();
                //cand ajunge la final ne returneaza null
                while((linie = reader.readLine()) != null){
                    builder.append(linie);
                }
                //prima data e obiect {}
                //JSONArray vector = new JSONArray(builder.toString());
                //acuma gasim obiect
                JSONObject object= new JSONObject(builder.toString());
                //luam informatiile din behavior
                JSONObject behavior = object.getJSONObject("behavior");
                JSONObject summary = behavior.getJSONObject("summary");
                JSONArray files = summary.getJSONArray("file_opened");

                for( int i =0; i<files.length();i++){
                    String file = files.get(i).toString();
                    fileOpened.add(file);
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return fileOpened;
        }
    }
}
