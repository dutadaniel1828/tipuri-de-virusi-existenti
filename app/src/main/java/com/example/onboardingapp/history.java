package com.example.onboardingapp;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

public class history extends AppCompatActivity {

    GridView gridView;
    String[] topic_title = {"A Brief History of Malware ","Top 10 threats of the future","Most Popular File Types Used by Malware ","5 Tips on Malware Protection"};
    int[] topic_content ={R.string.history,R.string.threats,R.string.type,R.string.tips};
    String[] colors_background = {"#0fbcf9","#ffa801","#ff5e57", "#05c46b"};
    String[] colors_backgroundText = {"#4bcffa","#ffd32a","#ff3f34","#0be881"};
    int[] topicImages = {R.drawable.malware_history,R.drawable.threats,R.drawable.file_type,R.drawable.tips};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        gridView = findViewById(R.id.gridView);
        CustomAdapter_gridView customAdapter = new CustomAdapter_gridView();

        gridView.setAdapter(customAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(),gridViewActivity.class);
                intent.putExtra("name",topic_title[position]);
                intent.putExtra("image",topicImages[position]);
                intent.putExtra("color",colors_background[position]);
                intent.putExtra("content",topic_content[position]);
                startActivity(intent);
            }
        });
    }

    private class CustomAdapter_gridView extends BaseAdapter {

        @Override
        public int getCount() {
            return topicImages.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view1 = getLayoutInflater().inflate(R.layout.grid_view_data,null);

            TextView name = view1.findViewById(R.id.textView_gridView);
            ImageView image = view1.findViewById(R.id.image_gridView);
            RelativeLayout layout_background = view1.findViewById(R.id.layout_background);

            name.setText(topic_title[position]);
            image.setImageResource(topicImages[position]);
            layout_background.setBackgroundColor(Color.parseColor(colors_background[position]));
            name.setBackgroundColor(Color.parseColor(colors_backgroundText[position]));

            return view1;
        }
    }
}
