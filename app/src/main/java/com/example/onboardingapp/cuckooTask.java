package com.example.onboardingapp;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class cuckooTask implements Parcelable {
    private int id;
    private int duration;
    private double score;
    private double location1;
    private double location2;
    private String malware_type;
    private String name;
    private String name_short;
    private String file_type;
    private int size;

    public cuckooTask(){}

    public cuckooTask(int id, int duration, double score, double location1, double location2, String malware_type, String name, String file_type, int size) {
        this.id = id;
        this.duration = duration;
        this.score = score;
        this.location1 = location1;
        this.location2 = location2;
        this.malware_type = malware_type;
        this.name = name;
        this.file_type = file_type;
        this.size = size;
    }

    protected cuckooTask(Parcel in){
        id = in.readInt();
        duration= in.readInt();
        score = in.readDouble();
        location1 = in.readDouble();
        location2 = in.readDouble();
        malware_type = in.readString();
        name = in.readString();
        file_type = in.readString();
        size = in.readInt();
    }

    public static final Creator<cuckooTask> CREATOR = new Creator<cuckooTask>() {
        @Override
        public cuckooTask createFromParcel(Parcel in) {
            return new cuckooTask(in);
        }

        @Override
        public cuckooTask[] newArray(int size) {
            return new cuckooTask[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(duration);
        dest.writeDouble(score);
        dest.writeDouble(location1);
        dest.writeDouble(location2);
        dest.writeString(malware_type);
        dest.writeString(name);
        dest.writeString(file_type);
        dest.writeInt(size);
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public double getLocation1() {
        return location1;
    }

    public void setLocation1(double location1) {
        this.location1 = location1;
    }

    public double getLocation2() {
        return location2;
    }

    public void setLocation2(double location2) {
        this.location2 = location2;
    }

    public String getMalware_type() {
        return malware_type;
    }

    public void setMalware_type(String malware_type) {
        this.malware_type = malware_type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFile_type() {
        return file_type;
    }

    public void setFile_type(String file_type) {
        this.file_type = file_type;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }


    @Override
    public String toString() {
        return "cuckooTask{" +
                "id=" + id +
                ", duration=" + duration +
                ", score=" + score +
                ", location1=" + location1 +
                ", location2=" + location2 +
                ", malware_type='" + malware_type + '\'' +
                ", name='" + name + '\'' +
                ", name_short='" + name_short + '\'' +
                ", file_type='" + file_type + '\'' +
                ", size=" + size +
                '}';
    }
}
