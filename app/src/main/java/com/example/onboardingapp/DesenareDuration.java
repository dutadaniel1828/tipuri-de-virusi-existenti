package com.example.onboardingapp;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

import java.util.List;

public class DesenareDuration extends View {

    List<Integer> durationList;
    List<String> nameList;

    public DesenareDuration(Context context, List<Integer> durationList, List<String> nameList) {
        super(context);
        this.durationList = durationList;
        this.nameList = nameList;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Paint p_chart = new Paint();
        p_chart.setStrokeWidth(100);

        Integer maxim = this.durationList.get(0);
        for(int i = 1; i < durationList.size(); i++){
            if(maxim < durationList.get(i))
                maxim = this.durationList.get(i);
        }

        Integer newMax = 500;

        Integer valoare = 0;
        Integer start = 200;
        Integer start_legend = 800;
        Integer intent = start;


        int culoare = 20;

        //pentru text
        Paint p_text = new Paint();
        p_text.setColor(Color.RED);
        p_text.setTextSize(50);


        for(int i = 0; i < durationList.size(); i++){
            p_chart.setColor(Color.rgb((300*culoare)%256,culoare%256,(20*culoare)%256));
            //punctul de unde pornim si unde ne oprim si ultimul este pensula noastra
            if( durationList.get(i) == maxim )
                valoare = 500;
            else
                valoare = (durationList.get(i) * newMax)/maxim;
            canvas.drawLine(intent,newMax-valoare,intent,newMax,p_chart);
            canvas.drawText(durationList.get(i).toString(),intent - 50,newMax+50,p_text);

            //pentru legenda
            canvas.drawLine(start,start_legend, start ,start_legend + 50,p_chart);

            String name = "";
            if (nameList.get(i).length() > 25)
                name = nameList.get(i).substring(0,22) + "...";
            else
                name = nameList.get(i);

            canvas.drawText(name,start +100,start_legend + 40,p_text);

            intent += 120;
            culoare +=50;
            start_legend += 100;
        }
    }
}
