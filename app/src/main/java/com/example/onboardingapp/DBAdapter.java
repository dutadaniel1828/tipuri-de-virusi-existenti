package com.example.onboardingapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class DBAdapter {
    private SQLiteDatabase database;
    private DBHelper helper;

    //Constructor
    public DBAdapter(Context ctx){ helper = new DBHelper(ctx,Constante.DBNAME, null, 1);}

    public void deleteRows(){
        database.delete(Constante.NUME_TABELA,null,null);
    }
    //deschidem conexiunea cu baza de date
    public void openConnection(){ database=helper.getWritableDatabase();}

    //inchidem conexiunea cu baza de date
    public void closeConenction(){ database.close(); }

    //Insert
    //pune by default o coloana id (in SQLite)
    public void insertReport(cuckooTask task){
        //verificam mai intai daca exista
        if(selectareTaskByName(task.getName()) == null) {

            ContentValues cv = new ContentValues();
            //pune sub forma cheie si valoarea tot ce trebuie sa punem in inregistrare
            cv.put(Constante.ID, task.getId());
            cv.put(Constante.DURATION, task.getDuration());
            cv.put(Constante.SCORE, task.getScore());
            cv.put(Constante.LOCATION1, task.getLocation1());
            cv.put(Constante.LOCATION2, task.getLocation2());
            cv.put(Constante.MALWARE_TYPE, task.getMalware_type());
            cv.put(Constante.NAME, task.getName());
            cv.put(Constante.FILE_TYPE, task.getFile_type());
            cv.put(Constante.SIZE, task.getSize());

            //inseram in tabela content value creeat
            database.insert(Constante.NUME_TABELA, null, cv);
        }
    }

    public cuckooTask selectareTaskByName(String nume){
        Cursor cursor = database.query(Constante.NUME_TABELA, new String[]{Constante.ID,Constante.DURATION,Constante.SCORE,Constante.LOCATION1,Constante.LOCATION2,Constante.MALWARE_TYPE,Constante.NAME,Constante.FILE_TYPE,Constante.SIZE},Constante.NAME+" =?",new String[]{nume}, null,null,null,null);

        //daca e diferit de null se muta pe primul camp
        if(cursor != null && cursor.moveToFirst()){
            cuckooTask task = new cuckooTask(cursor.getInt(0),cursor.getInt(1),cursor.getDouble(2),cursor.getDouble(3),cursor.getDouble(4),cursor.getString(5),cursor.getString(6),cursor.getString(7),cursor.getInt(8));
            return task;
        }
        return null;
    }

    public cuckooTask selectareTaskById(int id){
        Cursor cursor = database.query(Constante.NUME_TABELA, new String[]{Constante.ID,Constante.DURATION,Constante.SCORE,Constante.LOCATION1,Constante.LOCATION2,Constante.MALWARE_TYPE,Constante.NAME,Constante.FILE_TYPE,Constante.SIZE},Constante.ID+" =?",new String[]{""+id}, null,null,null,null);

        //daca e diferit de null se muta pe primul camp
        if(cursor != null && cursor.moveToFirst()){
            cuckooTask task = new cuckooTask(cursor.getInt(0),cursor.getInt(1),cursor.getDouble(2),cursor.getDouble(3),cursor.getDouble(4),cursor.getString(5),cursor.getString(6),cursor.getString(7),cursor.getInt(8));
            return task;
        }
        return null;
    }

    public List<cuckooTask> selectToateRapoartele(){
        //se creeaza cursorul pentru a lua toate coloanele
        Cursor cursor = database.query(Constante.NUME_TABELA,
                new String[]{Constante.ID,
                        Constante.DURATION,
                        Constante.SCORE,
                        Constante.LOCATION1,
                        Constante.LOCATION2,
                        Constante.MALWARE_TYPE,
                        Constante.NAME,
                        Constante.FILE_TYPE,
                        Constante.SIZE},
                null,
                null,
                null,
                null,
                null,
                null);
        List<cuckooTask> lista = new ArrayList<>();
        if(cursor !=null && cursor.moveToFirst()){
            //cat timp il avem pe urmatorul il adaugam in lista
            do{
                lista.add(new cuckooTask(cursor.getInt(0),cursor.getInt(1),cursor.getDouble(2),cursor.getDouble(3),cursor.getDouble(4),cursor.getString(5),cursor.getString(6),cursor.getString(7), cursor.getInt(8)));
            }while(cursor.moveToNext());
        }
        return lista;
    }
}
